create database m06store; 

CREATE TABLE Categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

CREATE TABLE Products (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    description TEXT,
    image VARCHAR(255),
    price DECIMAL(10,2),
    category_id INT,
    favorite BOOLEAN,
    FOREIGN KEY (category_id) REFERENCES Categories(id)
);

INSERT INTO Categories (id, name) VALUES 
(1, 'Home Decor'),
(2, 'Furniture'),
(3, 'Kitchen Appliances');


INSERT INTO Products (name, description, image, price, category_id, favorite)
VALUES
('Billy Bookcase', 'Ideal for organizing space and storing books.', 'assets/armario.png', 99.99, 1, false),
('Kallax Shelf', 'Crafted from high-quality materials.', 'assets/comoda.png', 79.99, 1, true),
('Hemnes Bed', 'Modern and versatile design.', 'assets/estante.png', 199.99, 2, false),
('Lack Coffee Table', 'Easy to assemble and maintain.', 'assets/sillon.png', 49.99, 2, true),
('Rens Sheepskin Rug', 'Functionality and quality at an affordable price.', 'assets/sofa.png', 7.99, 3, false),
('Ikea PS Lomsk Swivel Chair', 'Ideal for adding comfort and style to any living space.', 'assets/cuna.png', 149.99, 3, true),
('Skurar Plant Pot', 'Stylish plant pot adds a touch of greenery to your home.', 'assets/comoda.png', 9.99, 3, false),
('Nutid Microwave Oven', 'Make cooking easier and more convenient.', 'assets/armario.png', 149.99, 1, true),
('Duktig Play Kitchen', 'Encourage imaginative play with this adorable play kitchen.', 'assets/estante.png', 79.99, 2, false),
('Ikea 365+ Cookware Set', 'Upgrade your kitchen with this durable and versatile cookware set.', 'assets/cuna.png', 5.99, 1, true)

CREATE TABLE Users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    telefono VARCHAR(20),
    gender VARCHAR(255),
    country VARCHAR(100),
    interests VARCHAR(255),
    accept BOOLEAN NOT NULL
);