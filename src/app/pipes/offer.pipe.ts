import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'offer'
})
export class OfferPipe implements PipeTransform {

  transform(value: any): string {
    //console.log(value);
    let num = parseInt(value.slice(1));
    //console.log(num);
        if (num< 10) {
      return value + " *special offer";
    }
    return value.toString(); 
  }

}
