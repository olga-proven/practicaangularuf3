import { CanActivateFn, Router } from '@angular/router';
import { AuthenticatorService } from '../services/authenticator.service';
import { inject } from '@angular/core';

export const loginGuardGuard: CanActivateFn = (route, state) => {
  console.log("en loginGuard");
  const auth= inject(AuthenticatorService);
  const router = inject(Router);

  if (auth.loginDone === true) {
    console.log("login true");
    return true;
  }
  else {
    //return false;
    return router.navigate(["/empresa"]).then(()=>false);
  }
};


