import { CanActivateFn, Router } from '@angular/router';
import { AuthenticatorService } from '../services/authenticator.service';
import { inject } from '@angular/core';

export const hideRegLoginGuard: CanActivateFn = (route, state) => {
  const auth= inject(AuthenticatorService);
  const router = inject(Router);
  
  if (auth.hideRegisterLogin != true) {
    console.log("login hide NO true");//este guard no funciona
    return true;
  }
  else {
    
    return router.navigate(["/empresa"]).then(()=>false);
  }
};
