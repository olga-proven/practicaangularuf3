import { Directive, ElementRef, Input, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input("appHighlight") totalSum!: number;


  constructor(private div: ElementRef) {

  }

  ngOnInit(): void {
    this.actualizar();
  }



  ngOnChanges(changes: SimpleChanges): void {
    this.actualizar();
  }

  actualizar(){
    if (this.totalSum>=200){
    this.div.nativeElement.style.color = "red";
    }}
  }
