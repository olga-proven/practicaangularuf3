import { Directive } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[appQuantityToBuy]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: QuantityToBuyDirective, multi: true }
  ]
})

export class QuantityToBuyDirective {
  private _control!: AbstractControl;

  constructor() { }

  validate(control: FormControl): ValidationErrors | null {
    //console.log(control);

    if (parseInt(control.value) > 0){
      return null;
    }else {
      return { 'notValid': true };
    }
  }

}
