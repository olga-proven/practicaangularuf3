import { Directive, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
@Directive({
  selector: '[appConfirmUsername]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ConfirmUsernameDirective, multi: true }
  ]})
export class ConfirmUsernameDirective {
  @Input() password!: string;
  private _control!: AbstractControl;

  constructor() { }

  ngOnInit(): void {
    
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    //console.log(this._control);
    //console.log(changes);
    if (this._control != undefined)
      this._control.updateValueAndValidity(); 
  }

  validate(control: FormControl): ValidationErrors | null{
    //console.log(this._control);
    if (this._control == undefined)
      this._control = control;
    //console.log(control.value);
    //console.log(this.password);

    if (control.value === this.password){
      return null;
    }else{
      return {'notEqualToPassword': true };
    }

    // if (control.value == 'pepe'){
    //   return null;
    // }else{
    //   return {'notPepe': true };
    // }
  }
}


