import { Directive } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
@Directive({
  selector: '[appValidatePhone]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ValidatePhoneDirective, multi: true }
  ]
})
export class ValidatePhoneDirective {
  private _control!: AbstractControl;

  constructor() { }

  validate(control: FormControl): ValidationErrors | null {
    //console.log(this._control);

    if ((control.value.startsWith("6") || control.value.startsWith("93")) && control.value.length==9){
      return null;
    }else {
      return { 'notValid': true };
    }
  }


}
