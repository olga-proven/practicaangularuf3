import { Component } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { AuthenticatorService } from 'src/app/services/authenticator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  user!:User;
  message:string;

  constructor(private auth:AuthenticatorService){
    this.user = new User()
    this.message="";


  }

  ngOnInit() {


    this.auth. errorLoginMessage.subscribe(message => {
     

      if (message==true){

      this.message = "Database error, try again later";
       }  })

  }

  doLogin(){
    console.log(this.user.username);
    console.log(this.user.password);
    this.auth.login(this.user.username,this.user.password);



  }
}
