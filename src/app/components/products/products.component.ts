import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/model/category.model';
import { Product } from 'src/app/model/product.model';
import { ManageProductsService } from 'src/app/services/manage-products.service';
import { ShoppingcartService } from 'src/app/services/shoppingcart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],

})
export class ProductsComponent {
  products: Product[] = [];
  categories: Category[] = [];
  favoriteProducts: Product[] = [];
  cp: number;
  itemsPerPage: number;
  favoriteProduct: Product;

  message: string;
  quantity: number;
  inputFilter: string;
  selectFilter: string;
  filteredProducts: Product[] = [];
  maxPrice: number;
  minPrice: number;
  rangeFilter: number;
  showFavouriteForm: boolean;
  cantidadInvalida: boolean = false;



  constructor(private manageProducts: ManageProductsService, private cart: ShoppingcartService) {
    this.products = [];
    this.favoriteProducts = [];
    this.cp = 1;
    this.itemsPerPage = 5;
    this.favoriteProduct = new Product;
    this.message = "";
    this.quantity = 0;
    this.inputFilter = "";
    this.selectFilter = "";
    this.rangeFilter = 0;
    this.filteredProducts = this.products;
    this.maxPrice = 0;
    this.minPrice = 100;
    this.showFavouriteForm = false;
    this.cantidadInvalida=false;
    this.categories= [];

  }

/* CONNECTION TO SERVER  */

/*     ngOnInit(): void {

      this.getCategories();
      this.getProducts();
      this.filteredProducts = this.products;

    }
    
        getCategories(){
          this.manageProducts.getCategoryListFromServer().subscribe((data2: any[]) => {
            console.log("Received data from server:", data2);
            this.categories = data2;
            
          });
        }
        getProducts() {
          this.manageProducts.getProductListFromServer().subscribe((data: any[]) => {
            console.log("Received data from server:", data);
        
            for (let index = 0; index < data.length; index++) {
              const product = new Product();
              product.id = data[index].id;
              console.log(product.id);
              product.name = data[index].name;
              product.description = data[index].description;
              product.image = data[index].image;
              product.price = data[index].price;
              product.favorite = data[index].favorite;
              product.category = this.categories[data[index].category];
              this.products.push(product);
      
              
            }
          });
        } */
        
        ngOnInit(): void {
          //this.manageProducts.getProductListFromServer();

          this.getProductList();
          //console.log("en products ig on init");
          // Initialization logic that depends on data-bound properties can go here.
          // This method is called after Angular has initialized data-bound properties.
        }
      
      
      

  getProductList() {
    //this.manageProducts.createProducts(30);
    //console.log("en get products list");
    this.products = this.manageProducts.getProductList();
    //console.log(this.products);
    this.filteredProducts = this.products;
    //console.log(this.filteredProducts);
   // console.log( this.products.length);

    /* for (let index = 0; index < this.products.length; index++) {
      let product = this.products[index];
      if (product.price > this.maxPrice) {
        this.maxPrice = product.price;
      };

    }
    for (let index = 0; index < this.products.length; index++) {
      let product = this.products[index];
      if (product.price < this.minPrice) {
        this.minPrice = product.price;
      };

    }
    console.log(this.maxPrice);
    console.log(this.minPrice);
    console.log(this.products);
 */
  }
  getFavoriteProductList() {


    this.products = this.manageProducts.getFavoriteProductList();
    //console.log(this.favoriteProducts);
  }


  askAddToFavorite(favoriteProduct: Product) {

    this.favoriteProduct = favoriteProduct;
    this.showFavouriteForm = true;
    console.log(this.showFavouriteForm);
    //console.log(favoriteProduct);
  }


  receiveMessage($event: any) {
    //console.log("en padre receive message");
    this.message = $event;
    //console.log(this.message);
    if (this.message == "add") {
      //console.log(this.favoriteProduct);
      this.manageProducts.makeFavoriteTrue(this.favoriteProduct);
      this.showFavouriteForm = false;
    }
    else {
      this.manageProducts.makeFavoriteFalse(this.favoriteProduct);
      this.showFavouriteForm = false;
    }
  }
  addToCart(favoriteProduct: Product, quantity: string) {
    //console.log(favoriteProduct);
    //console.log("cantidad" +quantity);
    this.favoriteProduct = favoriteProduct;
    //console.log("en add to cart");
    let quantity2 = parseInt(quantity);
    if (this.validateQuantity(quantity2)){
      this.cantidadInvalida=true;
 
      this.cart.addProductToCart(this.favoriteProduct, quantity2);
    }
    
    //console.log(this.quantity);
    //console.log(this.favoriteProduct);
    

  }


  validateQuantity(quantity: number) {
    //let quantity2 = parseInt(quantity);
    if (quantity > 0) { 
      return true; }
    else {
      return false;
    }
  }

  filterName() {

    
    this.filteredProducts = this.products.filter((product: { name: string }) => {
      if (product.name.indexOf(this.inputFilter) != -1) {
        return true;
      }
      return false;
    });
  }

  filterSelect() {
    //console.log(this.selectFilter);
    this.filteredProducts = this.products.filter((product: { category: Category }) => {
      if (product.category.name.indexOf(this.selectFilter) != -1) {
        return true;
      }
      return false;
    });
  }
  filterRange() {
    //console.log(this.rangeFilter);
    this.filteredProducts = this.products.filter((product: { price: number }) => {
      if (product.price <= this.rangeFilter) {
        return true;
      }
      return false;
    });
  }

  filter() {
    //console.log(this.inputFilter);
    //console.log(this.selectFilter);
    //console.log(this.rangeFilter);
 
    //console.log(this.selectFilter);
    this.filteredProducts = this.products.filter((product: Product) => {
      let filterOK = true;

      if (this.rangeFilter!=0){//si el filtro de rango esta usado{
        if( product.price > this.rangeFilter){
          filterOK = false;
        }
         }
         if (this.selectFilter!=""){//si el filtro de rango esta usado{
          if( product.category.name !=this.selectFilter){
            filterOK = false;
          }
           }
           if (this.inputFilter!=""){//si el filtro de rango esta usado{
            if( product.name.indexOf(this.inputFilter) == -1){
              filterOK = false;
            }
             }

      return filterOK;


      return product.name.indexOf(this.inputFilter) != -1 &&
        product.category.name.indexOf(this.selectFilter) != -1 &&
        product.price <= this.rangeFilter;
    });
  }


/*   this.filteredProducts = this.products.filter((product: Product) => {
    return product.name.indexOf(this.inputFilter) != -1 &&
      product.category.name.indexOf(this.selectFilter) != -1 &&
      product.price <= this.rangeFilter;
  }); */
}


