import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent {

cookieValue: any;
  greeting: number;

  constructor(private cookieService: CookieService) {
    this.greeting = 1;
    this.cookieValue = "";
/* 
    if (!this.cookieValue) {
      
      this.cookieService.set('Test', 'hola', { expires: 1});
      this.cookieValue = 'hola';
    } */
  }
  

  ngOnInit(): void {
    console.log(this.greeting);
    console.log(this.cookieValue);
    this.cookieService.set('Test', 'hola', { expires: 5});
    //this.greeting = 0;
/*     if (this.cookieValue === 'hola') {
      
    } */
  }
}