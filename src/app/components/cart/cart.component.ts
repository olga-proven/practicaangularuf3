import { Component, SimpleChanges } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { ShoppingcartService } from 'src/app/services/shoppingcart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {
  products: { product: Product, quantity: number }[] = []; 
  totalSum: number;
  constructor(private cart:ShoppingcartService){
    this.products = [];
    this.totalSum=0;




  }
  ngOnInit(): void {
    this.cart.messageListProducts.subscribe(message => {
      //console.log("message "+ JSON.stringify(message));
      this.products=Object.values(message);
      //console.log(this. products);
 
    })

 
  }

  ngOnChanges(changes: SimpleChanges): void{
    
  }

  calculateTotalSum(){
    this.totalSum=0;
    for (let index = 0; index <this.products.length; index++) {
      //console.log(this.totalSum);
      //console.log(this.products[index].quantity);
      //console.log(this.products[index].product.price);

      this.totalSum += (this.products[index].quantity *  this.products[index].product.price);
      //console.log(this.totalSum);
      
    }
    return this.totalSum
  }

  deleteProduct(product:{ product: Product, quantity: number }){
    
    this.cart.deleteProduct(product);

  }

  updateQuantity(product:{ product: Product, quantity: number }){
//console.log(product);
this.cart.updateQuantity(product);
  }
}
