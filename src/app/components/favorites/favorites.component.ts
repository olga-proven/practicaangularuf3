import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { Product } from 'src/app/model/product.model';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css'],

})
export class FavoritesComponent {
  @Input() favoriteProduct: Product;
  @Output() messageToProduct = new EventEmitter<string>();
  //message: string = "add";
  //showForm: boolean = true;
  //@Input() showFavouriteForm: boolean;


  constructor() {
    this.favoriteProduct = new Product;
    //this.showFavouriteForm = true;

  }

  ngOnInit() {
    //console.log(this.showFavouriteForm);
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  sendMessage() {
    //console.log("en send message");
    this.messageToProduct.emit("add");
  }

  notAddToFavourites() {
    //console.log("notAddToFavourites");
    this.messageToProduct.emit("cancel");
    //console.log(this.favoriteProduct)

    //hay que pensar algo 

  }
}

