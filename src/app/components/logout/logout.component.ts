import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticatorService } from 'src/app/services/authenticator.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {

  constructor(private router: Router, private auth:AuthenticatorService){}



doNotLogout(){
  this.router.navigate([""]); 
}
doLogout(){
  this.auth.doLogOut();

}

}
