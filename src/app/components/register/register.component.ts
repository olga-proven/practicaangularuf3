import { Component, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { AuthenticatorService } from 'src/app/services/authenticator.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  countries:string[]=["Spain","France","UK","Germany", "Italy"];
  genders:string[]=["Female","Male"];
  interests:string[]=["Travel","Sports","Music","Movies", "Books"];

  user!:User;
  confirmPasswordVariable:string;
  users: User[] = [];
  message:string;
  //@Output() goToLogin= new EventEmitter<boolean>();



  constructor(private auth:AuthenticatorService){
    this.user = new User()
    this.confirmPasswordVariable="";
    this.users=[];
    this.message="";

  }

  ngOnInit() {


    this.auth. errorRegisterMessage.subscribe(message => {
      //console.log("message "+ message);

      if (message==true){

      this.message = "Database error, try again later";
       }  })

  }

  addNewUser(){
        
      // Add the user to the list
      this.users.push(this.user);
     
        console.log(this.user);
      this.auth.register(this.user);
      //this.auth.showLogin(true);
    //this.users.push(this.user);

    //console.log(this.users);
    //this.goToLogin.emit(true); 
    //}
 } 

 addInterest(interest:string){
  this.user.interests.push(interest);


 }
}
