import { Component, Input, SimpleChanges } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { AuthenticatorService } from 'src/app/services/authenticator.service';
import { ShoppingcartService } from 'src/app/services/shoppingcart.service';

@Component({
  selector: 'app-minicart',
  templateUrl: './minicart.component.html',
  styleUrls: ['./minicart.component.css']
})
export class MinicartComponent {

  //@Input() miniProducts: Product[] = [];
  miniProductsList: { product: Product, quantity: number }[] = []; 
  show:boolean;

  constructor(private cart:ShoppingcartService,private auth: AuthenticatorService){
    this.miniProductsList = [];
    this.show=false;



  }
  ngOnInit(): void {
    //console.log(this. miniProductsList);
    //this. miniProductsList=this.cart.getProductCart(); 
    
    this.cart.messageListProducts.subscribe(message => {
      //console.log("message "+ JSON.stringify(message));
      this.miniProductsList=Object.values(message);
      //console.log(this. miniProductsList);

      
    })

    this.auth.logOutMessage.subscribe(message => {
      //console.log("message "+ message);

      if (message==true){

      this.show = false;
}
    })
    this.auth.loginMessage.subscribe(message => {
      //console.log("message "+ message);

      if (message==true){

      this.show = true;
}
    })



  }

}

