import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProvinciaValidator } from 'src/app/model/province.model';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent {
    provincias:string[]=["Barcelona", "Tarragona", "Girona"];
    consultas:string[]=["Information", "Purchase", "Return",""];
    contactForm: FormGroup;

    constructor() {
      this.contactForm = new FormGroup({
        'name': new FormControl("", [
          Validators.required,
          Validators.minLength(6),
          Validators.pattern('[a-zA-Z ]*')
        ]),
        'queryType': new FormControl("", [
          Validators.required,
        
        ]),
        'province': new FormControl("",  [
          Validators.required,ProvinciaValidator.validate
       
        
        ]),
        
        'policy': new FormControl("", [
          Validators.requiredTrue,
        
        ])
      });
    }




    saveContactData(){
      console.log(this.contactForm.value.name);
      console.log(this.contactForm.value.queryType);
      console.log(this.contactForm.value.province);
      console.log(this.contactForm.value.policy);

    }

}
