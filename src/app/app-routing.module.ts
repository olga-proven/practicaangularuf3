import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { loginGuardGuard } from './guards/login-guard.guard';
import { hideRegLoginGuard } from './guards/hide-reg-login.guard';

const routes: Routes = [
  
{path:"",
component:EmpresaComponent},

{path:"empresa",
component:EmpresaComponent},

{path:"login",
/* canActivate:[hideRegLoginGuard], */

component:LoginComponent},

{path:"register",
/* canActivate:[hideRegLoginGuard], */
component:RegisterComponent},

{path:"products",
component:ProductsComponent},

{path:"cart",
canActivate:[loginGuardGuard],
component:CartComponent},

{path:"logout",
canActivate:[loginGuardGuard],
component:LogoutComponent},

{path:"contactform",
canActivate:[loginGuardGuard],
component:ContactFormComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
