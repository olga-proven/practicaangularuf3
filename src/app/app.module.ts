import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { LogoutComponent } from './components/logout/logout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmUsernameDirective } from './directives/confirm-username.directive';
import { ValidatePhoneDirective } from './directives/validate-phone.directive';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { QuantityToBuyDirective } from './directives/quantity-to-buy.directive';
import { MinicartComponent } from './components/minicart/minicart.component';
import {CookieService} from 'ngx-cookie-service';
import { OfferPipe } from './pipes/offer.pipe';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { HighlightDirective } from './directives/highlight.directive';
import { HttpClientModule } from '@angular/common/http';
import { ProvinciaValidator } from './model/province.model';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProductsComponent,
    CartComponent,
    EmpresaComponent,
    LogoutComponent,
    MinicartComponent,

    ConfirmUsernameDirective,
     ValidatePhoneDirective,
     FavoritesComponent,
     QuantityToBuyDirective,
     OfferPipe,
     ContactFormComponent,
     HighlightDirective,
  
     
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    HttpClientModule,
    
  ],
  providers:  [CookieService,ProvinciaValidator],
  bootstrap: [AppComponent]
})
export class AppModule { }
