import { Injectable } from '@angular/core';
import { User } from '../model/user.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatorService {

  private users: User[] = [];
  user!: User;
  loginDone:boolean;
  hideRegisterLogin:boolean;


  private messageRegister = new BehaviorSubject(false);
  registerMessage = this.messageRegister.asObservable();

  private messageLogin = new BehaviorSubject(false);
  loginMessage = this.messageLogin.asObservable();

  private messageLogOut = new BehaviorSubject(false);
  logOutMessage = this.messageLogOut.asObservable();

  private errorRegister = new BehaviorSubject(false);
  errorRegisterMessage = this.errorRegister.asObservable();

  private errorLogin = new BehaviorSubject(false);
  errorLoginMessage = this.errorLogin.asObservable();

  constructor(private http: HttpClient) {

    this.users= [];
    this.user = new User();
    this.loginDone=false;
    this.hideRegisterLogin=false;
   }

  showLogin(registerState: boolean) {
    this.messageRegister.next(registerState);
  }

  showLogout(loginState: boolean) {
    this.messageLogin.next(loginState);
  }

  finishLogOut(logOutState: boolean) {
    this.messageLogOut.next(logOutState);
  }

  showErrorRegisterMessage(message: boolean) {
    this.errorRegister.next(message);
  }
  showErrorLoginMessage(message: boolean) {
    this.errorLogin.next(message);
  }

  login(usernameLogin:string, passwordLogin:string) {
    console.log("en servicio auth login");

/*     for (let i = 0; i < this.users.length; i++) {
      if ((this.users[i].username ==usernameLogin) &&(this.users[i].password==passwordLogin)){
        this.showLogout(true);
        this.loginDone=true;
      }
    } */
    let login = {
      username: usernameLogin,
      password: passwordLogin,
    }
    this.http.post('http://localhost:3000/loginUser',login).subscribe(
      (data) => { 
        console.log(data);
        if (data === true){
          this.showLogout(true);
          this.loginDone=true;
          this.hideRegisterLogin=true;
          return true;}
        else{
          this.showErrorLoginMessage(true);
          return false;
        }
      
      },
      );
    

  }

  register(newUser: User) {
    this.http.post('http://localhost:3000/saveUser',newUser).subscribe(
      (data) => { 
        console.log(data);
        if (data === true){
          this.showLogin(true);
          return true;}
        else{
          this.showErrorRegisterMessage(true);
          return false;
        }
      
      },
      );
 /*    this.saveUser(newUser).subscribe(
      (data) => { 
        console.log(data);
      
      },
      ); */
   
    //console.log(result);
    
      this.users.push(newUser);
    //console.log("en servicio auth register");
    ///console.log(newUser);
    //console.log(newUser.password);
    
    //console.log(this.users);
    //for (let i = 0; i < this.users.length; i++) {
      //console.log(this.users[i]);
      //console.log(this.users[i].username);
      //console.log(this.users[i].password);
     } 

  saveUser(user: User): Observable<any> {
    console.log("en save user");
    console.log(user);
    return this.http.post('http://localhost:3000/saveUser',user);
  }



  doLogOut() {
    //console.log("en servicio auth doLogOut");
    this.finishLogOut(true);
  }


}





