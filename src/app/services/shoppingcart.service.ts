import { Injectable } from '@angular/core';
import { Product } from '../model/product.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingcartService {
  //productsInCart: Product[] = [];
  productToBuy:Product;
  productsInCart: { product: Product, quantity: number }[] = []; 

  
  //private listProducts = new BehaviorSubject<Product[]>([]);
  private listProducts = new BehaviorSubject<{ product: Product, quantity: number }[]>([]);

  messageListProducts = this.listProducts.asObservable();

  constructor() { 
    //this.productsInCart= [];
    this.productToBuy=new Product;
    this.productsInCart = []; 
  }


  ngOnInit() {
    
    //console.log(this.productsInCart);

  }

  sendInfo(products:  { product: Product, quantity: number }[] = []    ) {
    this.listProducts.next(this.productsInCart);
    console.log("send info sent")
  }

  addProductToCart(product:Product,quantity:number){ 
    console.log(product);
    console.log(quantity);
    
    let productQuantity = { product: product, quantity: quantity };
   

    let found = false;
    
    for (let index = 0; index < this.productsInCart.length; index++) {
      if (this.productsInCart[index].product.id === product.id){
        this.productsInCart[index].quantity = Number(this.productsInCart[index].quantity) + Number(quantity);
        found = true;
        break;}
   
    }

    if (!found) {

      this.productsInCart.push(productQuantity);
    }
    console.log(this.productsInCart);
    
    this.sendInfo(this.productsInCart); 
  }

  getProductCart(){ 
    return this.productsInCart;
    
  }

  deleteProduct(product:{ product: Product, quantity: number }){
    console.log(product);
    for (let index = 0; index < this.productsInCart.length; index++) {
     
      if (this.productsInCart[index].product.id === product.product.id){
        this.productsInCart.splice(index,1);
      }

      
    }
    this.sendInfo(this.productsInCart); 
  }
  

  updateQuantity(product:{ product: Product, quantity: number }){
    console.log(product);
    for (let index = 0; index < this.productsInCart.length; index++) {
     
      if (this.productsInCart[index].product.id === product.product.id){
        this.productsInCart[index].quantity= product.quantity;
      }

      
    }
    this.sendInfo(this.productsInCart); 
  }



}
        
