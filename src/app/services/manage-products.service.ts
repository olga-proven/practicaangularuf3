import { Injectable } from '@angular/core';
import { Product } from '../model/product.model';
import { Category } from '../model/category.model';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ManageProductsService {
  productsList: Product[] = [];
  categoryList: Category[] = [];
  favoriteProductsList: Product[] = [];


  constructor(private http: HttpClient) {
    this.productsList = [];
    this.favoriteProductsList = [];
    this.categoryList = [];

  }

  getProductListFromServer(): any {
    //console.log("en getProductListFromServer");
    this.http.get('http://localhost:3000/products').subscribe(
      (data: any) => {
        //console.log(data);
        this.http.get('http://localhost:3000/categories').subscribe(
          (data2: any) => {
            //console.log(data2);
            //console.log(data);
            for (let index = 0; index < data2.length; index++) {
              const category = new Category(data2[index].id, data2[index].name);
              this.categoryList[data2[index].id] = category;
            }
            for (let index = 0; index < data.length; index++) {
              const product = new Product();
              product.id = data[index].id;
              //console.log(product.id);
              product.name = data[index].name;
              product.description = data[index].description;
              product.image = data[index].image;
              product.price = data[index].price;
              product.favorite = data[index].favorite;
              product.category = this.categoryList[data[index].category_id]
              this.productsList.push(product);
            }
            //console.log(this.productsList);
            //console.log(this.productsList[0]);
          }


        );
      }
    );
  }

  /*     getCategoryByIdFromServer(categoryId: number): Observable<any> {
        console.log("en getCategoryByIdFromServer");
        return ;
      } */

  getCategoryListFromServer(): Observable<any> {
    console.log("en getCategoryListFromServer");
    return this.http.get('http://localhost:3000/categories');
  }





  createProducts(numberOfProducts: number) {

    const productNames = [
      'Billy Bookcase',
      'Kallax Shelf',
      'Hemnes Bed',
      'Lack Coffee Table',
      'Rens Sheepskin Rug',
      'Ikea PS Lomsk Swivel Chair',
      'Skurar Plant Pot',
      'Nutid Microwave Oven',
      'Duktig Play Kitchen',
      'Ikea 365+ Cookware Set'
    ];
    const imageNames = [
      'assets/armario.png',
      'assets/comoda.png',
      'assets/estante.png',
      'assets/sillon.png',
      'assets/sofa.png',
      'assets/cuna.png'
    ];
    const productDescriptions = [
      "This product is ideal for organizing your space and storing books, files, and other personal items. It features adjustable shelves to suit your storage needs.",
      "Crafted from high-quality materials, this product is designed to last and withstand daily use.",
      "With a modern and versatile design, this product fits perfectly into a variety of decor styles and space configurations.",
      "Easy to assemble and maintain, this product comes with clear and simple instructions for easy installation. Additionally, its low-maintenance design makes it perfect for a busy lifestyle.",
      "Get the functionality and quality you need at an affordable price with this product. It's an excellent choice for those looking for quality without compromising their budget."
    ];


    for (let i = 0; i < numberOfProducts; i++) {
      const product = new Product();
      product.id = i + 1;
      product.name = productNames[Math.floor(Math.random() * productNames.length)];
      product.description = productDescriptions[Math.floor(Math.random() * productDescriptions.length)];
      product.image = imageNames[Math.floor(Math.random() * imageNames.length)];
      product.price = Math.floor(Math.random() * 100) + 1;
      product.category = new Category(1, this.findCategory(product.name));
      product.favorite = false;
      this.productsList.push(product);
      //console.log(Math.floor(Math.random() * 3));
    }
  }
  getProductList():any {
    this.getProductListFromServer();
    return this.productsList;
  }

  findCategory(name: string): string {

    const categoryNameList = ['Furniture', 'Home Decor', 'Kitchen Appliances'];
    const furnitureNames = ['Billy Bookcase', 'Kallax Shelf', 'Hemnes Bed', 'Lack Coffee Table'];
    const homeDecorNames = ['Rens Sheepskin Rug', 'Ikea PS Lomsk Swivel Chair', 'Skurar Plant Pot'];
    const kitchenAppliancesNames = ['Nutid Microwave Oven', 'Duktig Play Kitchen', 'Ikea 365+ Cookware Set'];

    if (furnitureNames.includes(name)) {
      return 'Furniture';
    } else if (homeDecorNames.includes(name)) {
      return 'Home Decor';
    } else if (kitchenAppliancesNames.includes(name)) {
      return 'Kitchen Appliances';
    } else {

      return "";
    }
  }

  getFavoriteProductList() {
    for (let i = 0; i < this.productsList.length; i++) {
      if (this.productsList[i].favorite = true) {
        this.favoriteProductsList.push(this.productsList[i])
      }
    }
    return this.favoriteProductsList;
  }


  makeFavoriteTrue(product: Product) {
    //console.log("en make fav true");
    //console.log(product);
    if (this.productsList.includes(product)) {
      product.favorite = true;
      //console.log(product);
    }
  }

  makeFavoriteFalse(product: Product) {
    //console.log("en make fav true");
    //console.log(product);
    if (this.productsList.includes(product)) {
      product.favorite = false;
      //console.log(product);
    }
  }


}