import { Category } from '../model/category.model';

export class Product {

    id: number;
    name: string;
    description: string;
    image: string;
    price: number;
    category: Category;
    favorite: boolean;
    //quantityToBuy:any;
    
    constructor() {
        this.id = 0;
        this.name = "";
        this.description = "";
        this.image = "";
        this.price = 0;
        this.category = new Category(0, ""); 
        this.favorite = false;
        //this.quantityToBuy=null;
    }
}
