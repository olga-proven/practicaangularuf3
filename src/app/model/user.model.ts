export class User {
    username: string;
    password: string;
    email: string;
    telefono: string;
    gender: string;
    country: string;
    interests: string[]; 
    
    accept: boolean;
  
    constructor() {
      this.username = "";
      this.password = "";
      this.email = "";
      this.telefono = "";
      this.gender = "";
      this.country = "";
      this.interests = []; 
      this.accept = false;
    }
  }
  