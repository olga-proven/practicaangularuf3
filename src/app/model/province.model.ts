import { AbstractControl, ValidatorFn } from '@angular/forms';

export class ProvinciaValidator {
  static validate(control: AbstractControl): { [key: string]: boolean } | null {
    const provincias = ['Barcelona', 'Tarragona', 'Girona'];
    //if (control.value && provincias.indexOf(control.value) === -1) {
        if (!provincias.includes(control.value)) {
      return { 'provinciaInvalida': true };
    }
    return null;
  }
}
