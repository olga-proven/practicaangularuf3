import { Component } from '@angular/core';
import { AuthenticatorService } from './services/authenticator.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'practicaAngularUF3';
  showLoginForm = false;
  showRegisterForm = true;
  showLogOut = false;
  showShoppingCart = false;
  showContactForm=false;
  showProducts=false;


  constructor(private auth: AuthenticatorService,private router: Router, private route: ActivatedRoute) {

  }
  ngOnInit() {

    this.auth.registerMessage.subscribe(message => {
      //console.log("message "+ message);

      if (message==true){

      this.showLoginForm = true;
      this.showRegisterForm = false;
      this.showLogOut = false;
      this.router.navigate(['login'], { relativeTo: this.route });}
    })


    this.auth.loginMessage.subscribe(message => {
      //console.log("message "+ message);

      if (message==true){

      this.showLoginForm = false;
      this.showRegisterForm = false;
      this.showLogOut = true;
      this.showShoppingCart = true;
      this.showContactForm = true;
      this.showProducts=true;
      this.router.navigate(['products'], { relativeTo: this.route });}
    })

    this.auth.logOutMessage.subscribe(message => {
      //console.log("message "+ message);

      if (message==true){

      this.showLoginForm = true;
      this.showRegisterForm = false;
      this.showLogOut = false;
      this.showShoppingCart = false;
      this.showContactForm = false;
      this.router.navigate([''], { relativeTo: this.route });}
    })
  }

}
